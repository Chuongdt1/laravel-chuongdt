<?php

namespace App\Repositories\Posts;

use App\Repositories\BaseRepository;

class PostsRepository extends BaseRepository implements PostsRepositoryInterface
{
    //Get the corresponding model
    public function getModel()
    {
        return \App\Models\Posts::class;
    }
}
