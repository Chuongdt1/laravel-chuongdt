<?php

namespace App\Http\Controllers;

use App\Repositories\Posts\PostsRepositoryInterface;
use App\Http\Requests\PostsRequest;
use App\Http\Resources\PostsResource;


class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $postsRepository;
    public function __construct(PostsRepositoryInterface $postsRepository)
    {
        $this->postsRepository = $postsRepository;
    }

    public function index()
    {
        return $this->postsRepository->getAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {
        $data = $request->all();
        $posts = $this->postsRepository->create($data);
        return response()->json([new PostsResource($posts), "Create succsess"], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = $this->postsRepository->getById($id);
        return response(new PostsResource($result), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostsRequest $request, $id)
    {
        $data = $request->all();
        $id = $this->postsRepository->update($id, $data);
        return response()->json([new PostsResource($id), "Update succsess"], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->postsRepository->delete($id);
        return response()->json(["Delete succsess"], 200);
    }
}
