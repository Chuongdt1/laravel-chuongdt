<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;

abstract class BaseRepository implements RepositoryInterface
{
    //Model wants to interact
    protected $model;

    //Initialization
    public function __construct()
    {
        $this->setModel();
    }

    //Get the corresponding model
    abstract public function getModel();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make(
            $this->getModel()
        );
    }

    /**
     * Get all
     * @return []
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get ById
     * @param int $id
     * @return $result{}
     */
    public function getById($id)
    {
        $result = $this->model->find($id);
        return $result;
    }

    /**
     * Create
     * @param array $attributes
     * @return mixed
     */
    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }

    /**
     * Update
     * @param int $id
     * @param array $attributes
     * @return $result{}
     */
    public function update($id, $attributes = [])
    {
        $result = $this->getById($id);
        if ($result) {
            $result->update($attributes);
            return $result;
        }

        return false;
    }

    /**
     * Delete
     * @param $id
     * @return boolean
     */
    public function delete($id)
    {
        $result = $this->getById($id);
        if ($result) {
            $result->delete();

            return true;
        }

        return false;
    }
}
