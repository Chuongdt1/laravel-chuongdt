<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:6|max:20',
            'title' => 'required|min:25|max:50',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'name.required' => 'The name field is required',
            'name.min' => 'Must not be less than 6 characters.',
            'name.max' => 'Must not be greater than 20 characters.',
            'title.required' => 'The title field is required',
            'title.min' => 'Must not be less than 25 characters.',
            'title.max' => 'Must not be greater than 50 characters.',
        ];
    }
}
